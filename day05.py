def a(polymer):
    polymer = list(polymer)
    reacted = True
    while reacted:
        reacted = False
        for i, (a, b) in enumerate(zip(polymer, polymer[1:])):
            if a.lower() == b.lower() and a != b:
                polymer.pop(i)
                polymer.pop(i)
                reacted = True
                break
    return len(polymer)


def b(polymer):
    letters = set(polymer.lower())
    return min(a(polymer.replace(l, "").replace(l.upper(), "")) for l in letters)


def main():
    polymer = open("input05.txt").read()
    print(b(polymer))


if __name__ == "__main__":
    main()
