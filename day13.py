from dataclasses import dataclass, field
import itertools
import re
from typing import Iterable, Tuple


@dataclass
class Vector2:
    x: int
    y: int

    def __add__(self, other):
        return Vector2(self.x + other.x, self.y + other.y)


@dataclass
class Cart:
    id: int
    position: Vector2
    direction: str
    next_turn: Iterable[str] = field(default_factory=lambda: itertools.cycle("lsr"))


directions = "^>v<"
dir_to_vec = {
    "^": Vector2( 0, -1),
    ">": Vector2( 1,  0),
    "v": Vector2( 0,  1),
    "<": Vector2(-1,  0),
}


def show_carts(tracks, carts):
    t = [list(t) for t in tracks]
    for cart in carts:
        t[cart.position.y][cart.position.x] = cart.direction
    print("\n".join(["".join(u) for u in t]))


def a(tracks, carts):
    for i in itertools.count():
        # show_carts(tracks, carts)
        carts = sorted(carts, key=lambda c: c.position.y)
        for cart in carts:
            cart.position += dir_to_vec[cart.direction]
            for other in carts:
                if cart is not other and cart.position == other.position:
                    return cart.position

            turn = ""
            if tracks[cart.position.y][cart.position.x] == "\\":
                turn = "r" if cart.direction in "<>" else "l"
            elif tracks[cart.position.y][cart.position.x] == "/":
                turn = "l" if cart.direction in "<>" else "r"
            elif tracks[cart.position.y][cart.position.x] == "+":
                turn = next(cart.next_turn)

            if turn == "l":
                cart.direction = directions[(directions.index(cart.direction) - 1) % 4]
            elif turn == "r":
                cart.direction = directions[(directions.index(cart.direction) + 1) % 4]


def b(tracks, carts):
    alive = {cart.id for cart in carts}
    for i in itertools.count():
        # show_carts(tracks, carts)
        carts = sorted(carts, key=lambda c: c.position.y)
        for cart in carts:
            if cart.id not in alive:
                continue
            cart.position += dir_to_vec[cart.direction]
            for other in carts:
                if other.id in alive and cart is not other and cart.position == other.position:
                    alive.remove(cart.id)
                    alive.remove(other.id)
                    continue
            turn = ""
            if tracks[cart.position.y][cart.position.x] == "\\":
                turn = "r" if cart.direction in "<>" else "l"
            elif tracks[cart.position.y][cart.position.x] == "/":
                turn = "l" if cart.direction in "<>" else "r"
            elif tracks[cart.position.y][cart.position.x] == "+":
                turn = next(cart.next_turn)

            if turn == "l":
                cart.direction = directions[(directions.index(cart.direction) - 1) % 4]
            elif turn == "r":
                cart.direction = directions[(directions.index(cart.direction) + 1) % 4]
        if len(alive) == 1:
            return [cart.position for cart in carts if cart.id in alive][0]


def main():
    tracks = open("input13.txt").read().split("\n")
    carts = []
    for y, row in enumerate(tracks):
        for x, l in enumerate(row):
            if l in "<>^v":
                carts.append(Cart(len(carts), Vector2(x, y), l))
        tracks[y] = row.replace("<", "-").replace(">", "-").replace("^", "|").replace("v", "|")
    print(b(tracks, carts))


if __name__ == "__main__":
    main()
