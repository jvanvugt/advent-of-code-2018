import copy
from collections import defaultdict
import time


def a(world):
    N = 50
    for i in range(10):
        old = copy.deepcopy(world)
        for y in range(N):
            for x in range(N):
                neighs = defaultdict(int)
                for xx in range(x - 1, x + 2):
                    for yy in range(y - 1, y + 2):
                        if 0 <= xx < N and 0 <= yy < N and not (xx == x and yy == y):
                            neighs[old[yy][xx]] += 1
                if world[y][x] == ".":
                    if neighs["|"] >= 3:
                        world[y][x] = "|"
                elif world[y][x] == "|":
                    if neighs["#"] >= 3:
                        world[y][x] = "#"
                elif world[y][x] == "#":
                    if not (neighs["#"] >= 1 and neighs["|"] >= 1):
                        world[y][x] = "."
    w = "\n".join("".join(w) for w in world)
    # print(w)
    # print("\n\n")
    return w.count("|") * w.count("#")


def b(world):
    N = 50
    minutes = 1000000000
    history = {}
    i = 0
    skipped = False
    while i < minutes:
        old = copy.deepcopy(world)
        for y in range(N):
            for x in range(N):
                neighs = defaultdict(int)
                for xx in range(x - 1, x + 2):
                    for yy in range(y - 1, y + 2):
                        if 0 <= xx < N and 0 <= yy < N and not (xx == x and yy == y):
                            neighs[old[yy][xx]] += 1
                if world[y][x] == ".":
                    if neighs["|"] >= 3:
                        world[y][x] = "|"
                elif world[y][x] == "|":
                    if neighs["#"] >= 3:
                        world[y][x] = "#"
                elif world[y][x] == "#":
                    if not (neighs["#"] >= 1 and neighs["|"] >= 1):
                        world[y][x] = "."
        i += 1
        w = "\n".join("".join(w) for w in world)
        if not skipped and w in history:
            cycle_length = i - history[w]
            left = minutes - i - 1
            i += (left // cycle_length) * cycle_length
            skipped = True
        else:
            history[w] = i
        # print(w)
        # print("\n\n")
        # time.sleep(1)
    return w.count("|") * w.count("#")


def main():
    world = [list(line) for line in open("input18.txt").read().splitlines()]
    print(b(world))


if __name__ == "__main__":
    main()
