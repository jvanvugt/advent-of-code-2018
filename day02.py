from collections import Counter


def a(box_ids):
    twos = threes = 0
    for box_id in box_ids:
        counts = Counter(box_id)
        twos += int(2 in counts.values())
        threes += int(3 in counts.values())
    return twos * threes


def b(box_ids):
    for i, box_a in enumerate(box_ids):
        for j, box_b in enumerate(box_ids):
            if i != j:
                diffs = 0
                for letter_a, letter_b in zip(box_a, box_b):
                    if letter_a != letter_b:
                        diffs += 1
                        if diffs == 2:
                            break
                if diffs == 1:
                    return "".join([l for l in box_a if l in box_b])


if __name__ == "__main__":
    with open("input02.txt") as f:
        box_ids = f.readlines()
    print(b(box_ids))
