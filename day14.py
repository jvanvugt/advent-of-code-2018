def a(n):
    scores = [3, 7]
    elves = [0, 1]
    while len(scores) < n + 10:
        new_score = str(scores[elves[0]] + scores[elves[1]])
        for s in new_score:
            scores.append(int(s))
        for i in range(len(elves)):
            elves[i] = (elves[i] + scores[elves[i]] + 1) % len(scores)

    return "".join([str(s) for s in scores[-10:]])


def b(n):
    scores = [3, 7]
    elves = [0, 1]
    query = list(map(int, str(n)))
    while scores[-len(query):] != query:
        new_score = str(scores[elves[0]] + scores[elves[1]])
        for s in new_score:
            scores.append(int(s))
            if scores[-len(query):] == query:
                return len(scores) - len(query) 
        for i in range(len(elves)):
            elves[i] = (elves[i] + scores[elves[i]] + 1) % len(scores)

    return len(scores) - len(query)


def main():
    print(b("290431"))


if __name__ == "__main__":
    main()
