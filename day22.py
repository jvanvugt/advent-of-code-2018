from functools import lru_cache
from collections import defaultdict
import heapq


@lru_cache(maxsize=100000)
def geo_index(x, y, depth, target_x, target_y):
    if x == y == 0:
        return 0
    if (x, y) == (target_x, target_y):
        return 0
    if y == 0:
        return x * 16807
    if x == 0:
        return y * 48271
    ero1 = erosion_level(x - 1, y, depth, target_x, target_y)
    ero2 = erosion_level(x, y - 1, depth, target_x, target_y)
    return ero1 * ero2


def erosion_level(x, y, depth, target_x, target_y):
    return (geo_index(x, y, depth, target_x, target_y) + depth) % 20183


@lru_cache(maxsize=100000)
def get_type(x, y, depth, target_x, target_y):
    rem = erosion_level(x, y, depth, target_x, target_y) % 3
    return ["rocky", "wet", "narrow"][rem]


RISKS = {"rocky": 0, "wet": 1, "narrow": 2}


def a(depth, target_x, target_y):
    total_risk = 0
    for x in range(target_x + 1):
        for y in range(target_y + 1):
            t = get_type(x, y, depth, target_x, target_y)
            total_risk += RISKS[t]
    return total_risk


TOOLS = ["torch", "climbing gear", "neither"]


def is_allowed(type_, tool):
    if type_ == "rocky":
        return tool in ("climbing gear", "torch")
    if type_ == "wet":
        return tool in ("climbing gear", "neither")
    if type_ == "narrow":
        return tool in ("torch", "neither")
    raise ValueError(type_)


def heuristic(x, y, target_x, target_y, tool):
    steps = abs(target_x - x) + abs(target_y - y)
    switch = int(tool != "torch") * 7
    return steps + switch


def neighbours(time, x, y, tool, depth, target_x, target_y):
    t = get_type(x, y, depth, target_x, target_y)
    for ntool in TOOLS:
        if is_allowed(t, ntool):
            new_state = time + 7, x, y, ntool
            yield new_state
    for nx, ny in [(x, y - 1), (x + 1, y), (x, y + 1), (x - 1, y)]:
        if nx < 0 or ny < 0:
            continue
        t = get_type(nx, ny, depth, target_x, target_y)
        if is_allowed(t, tool):
            yield time + 1, nx, ny, tool


def b(depth, target_x, target_y):
    # with A*
    q = [(heuristic(0, 0, target_x, target_y, "torch"), 0, 0, 0, "torch")]
    visited = set(q[0][1:])
    while len(q) > 0:
        _, time, x, y, tool = heapq.heappop(q)
        if x == target_x and y == target_y and tool == "torch":
            return time
        for new_state in neighbours(time, x, y, tool, depth, target_x, target_y):
            if new_state not in visited:
                visited.add(new_state)
                _, nx, ny, ntool = new_state
                g_score = time
                f_score = g_score + heuristic(nx, ny, target_x, target_y, ntool)
                heapq.heappush(q, (f_score, *new_state))


def main():
    open("input22.txt")
    depth, target_x, target_y = 10647, 7, 770
    print(b(depth, target_x, target_y))


if __name__ == "__main__":
    main()
