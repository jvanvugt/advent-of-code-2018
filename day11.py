import itertools


def a(serial):
    grid = [[None for _ in range(300)] for _ in range(300)]
    for x in range(1, 301):
        for y in range(1, 301):
            rack_id = x + 10
            power_level = rack_id * y
            power_level += serial
            power_level *= rack_id
            power_level = (power_level // 100) % 10
            power_level -= 5
            grid[y - 1][x - 1] = power_level

    max_coords = max(itertools.product(range(1, 301 - 2), repeat=2),
                     key=lambda c: sum(grid[c[1] - 1 + i][c[0] - 1 + j]
                                       for i in range(3)
                                       for j in range(3)))
    return max_coords


def b(serial):
    grid_size = 300
    grid = [[None for _ in range(grid_size)] for _ in range(grid_size)]
    for x in range(1, grid_size + 1):
        for y in range(1, grid_size + 1):
            rack_id = x + 10
            power_level = rack_id * y
            power_level += serial
            power_level *= rack_id
            power_level = (power_level // 100) % 10
            power_level -= 5
            grid[y - 1][x - 1] = power_level

    max_coord = (-1, -1, -1)
    max_value = float("-inf")
    values = [g.copy() for g in grid]
    import tqdm
    for s in tqdm.trange(2, grid_size + 1):
        for i in range(grid_size - s + 1):
            for j in range(grid_size - s + 1):
                values[i][j] += sum(grid[i + s - 1][j + k] for k in range(s))
                values[i][j] += sum(grid[i + k][j + s - 1] for k in range(s))
                values[i][j] -= grid[i + s - 1][j + s - 1]
                if values[i][j] > max_value:
                    max_value = values[i][j]
                    max_coord = (j + 1, i + 1, s)

    return max_coord, max_value


def main():
    serial = 2568
    print(b(serial))


if __name__ == "__main__":
    main()
