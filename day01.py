import itertools


def a(changes):
    return sum(changes)


def b(changes):
    past_freqs = set()
    curr_freq = 0
    changes = itertools.cycle(changes)
    while curr_freq not in past_freqs:
        past_freqs.add(curr_freq)
        curr_freq += next(changes)
    return curr_freq


if __name__ == "__main__":
    with open("input01.txt") as f:
        changes = list(map(int, f.readlines()))
    print(b(changes))
