from collections import defaultdict
import dataclasses
import re


@dataclasses.dataclass
class SleepTime:
    guard_id: int
    start: int
    end: int


def create_sleep_time_dict(sleep_logs):
    sleep_time_per_guard = defaultdict(lambda: defaultdict(int))
    for log in sleep_logs:
        for time in range(log.start, log.end):
            sleep_time_per_guard[log.guard_id][time] += 1
    return sleep_time_per_guard


def a(sleep_time_per_guard):
    max_guard = max(sleep_time_per_guard, key=lambda guard_id: sum(sleep_time_per_guard[guard_id].values()))
    max_minute = max(sleep_time_per_guard[max_guard], key=sleep_time_per_guard[max_guard].__getitem__)
    return max_guard * max_minute


def b(sleep_time_per_guard):
    max_guard = max(sleep_time_per_guard, key=lambda guard_id: max(sleep_time_per_guard[guard_id].values()))
    max_minute = max(sleep_time_per_guard[max_guard], key=sleep_time_per_guard[max_guard].__getitem__)
    return max_guard * max_minute


def main():
    sleep_logs = sorted(open("input04.txt"))
    current_guard = -1
    fall_asleep = -1
    sleep_times = []
    for log in sleep_logs:
        log = log.strip()
        guard_id = re.findall(r"#\d+", log)
        if len(guard_id) == 1:
            current_guard = int(guard_id[0][1:])
        elif log.endswith("falls asleep"):
            fall_asleep = int(re.findall(r":\d+", log)[0].lstrip(":"))
        elif log.endswith("wakes up"):
            wake_up = int(re.findall(r":\d+", log)[0].lstrip(":"))
            sleep_times.append(SleepTime(current_guard, fall_asleep, wake_up))
    sleep_time_per_guard = create_sleep_time_dict(sleep_times)
    print(a(sleep_time_per_guard))


if __name__ == "__main__":
    main()
