using Debugger

function arithr(op)
    function helpera(registers, instr)
        registers[instr[4] + 1] = op(registers[instr[2] + 1], registers[instr[3] + 1])
    end
end

function arithi(op)
    function helperb(registers, instr)
        registers[instr[4] + 1] = op(registers[instr[2] + 1], instr[3])
    end
end

function arithir(op)
    function helperc(registers, instr)
        registers[instr[4] + 1] = op(instr[2], registers[instr[3] + 1])
    end
end


addr = arithr(+)
addi = arithi(+)
mulr = arithr(*)
muli = arithi(*)
banr = arithr(&)
bani = arithi(&)
borr = arithr(|)
bori = arithi(|)

function setr(registers, instr)
    registers[instr[4] + 1] = registers[instr[2] + 1]
end

function seti(registers, instr)
    registers[instr[4] + 1] = instr[2]
end

gtir = arithir(>)
gtri = arithi(>)
gtrr = arithr(>)
eqir = arithir(==)
eqri = arithi(==)
eqrr = arithr(==)

all_ops = Dict("addr" => addr, "addi" => addi, "mulr" => mulr, "muli" => muli, "banr" => banr,
               "bani" => bani, "borr" => borr, "bori" => bori, "setr" => setr, "seti" => seti,
               "gtir" => gtir, "gtri" => gtri, "gtrr" => gtrr, "eqir" => eqir, "eqri" => eqri,
               "eqrr" => eqrr)

function a()
    contents = open("input19.txt") do file
        read(file, String)
    end
    lines = split(contents, "\n")
    ip_register = parse(Int32, split(lines[1])[end])
    lines = lines[2:end]
    ip = 0
    registers = zeros(Int64, 6)
    while true
        if ip >= length(lines) || ip < 0
            break
        end
        parts = split(lines[ip + 1])
        op = all_ops[parts[1]]
        parts[1] = "0"
        args = map(x->parse(Int32, x), parts)
        registers[ip_register + 1] = ip
        op(registers, args)
        ip = registers[ip_register + 1]
        ip += 1
        # println(registers, " ", ip)
    end
    registers[1]
end

function b()
    contents = open("input19.txt") do file
        read(file, String)
    end
    lines = split(contents, "\n")
    ip_register = parse(Int32, split(lines[1])[end])
    lines = lines[2:end]
    ip = 0
    registers = zeros(Int64, 6)
    registers[1] = 1
    while true
        if ip >= length(lines) || ip < 0
            break
        end
        parts = split(lines[ip + 1])
        op = all_ops[parts[1]]
        parts[1] = "0"
        args = map(x->parse(Int32, x), parts)
        registers[ip_register + 1] = ip
        op(registers, args)
        ip = registers[ip_register + 1]
        ip += 1
        println(registers, " ", ip)
    end
    registers[1]
end

println(b())
