def min_dist(point, coords):
    dists = [abs(point[0] - x) + abs(point[1] - y) for x, y in coords]
    min_dist = min(dists)
    argmins = [d for d in dists if d == min_dist]
    if len(argmins) > 1:
        return -1
    return dists.index(min_dist)


def a(coords):
    max_x = max(c[0] for c in coords) + 1
    max_y = max(c[1] for c in coords) + 1
    grid = [[-1 for _ in range(max_y)] for _ in range(max_x)]
    valids = set(range(len(coords)))
    for x in range(max_x):
        for y in range(max_y):
            grid[x][y] = min_dist((x, y), coords)
            if (x == 0 or y == 0 or x == max_x - 1 or y == max_y - 1) and grid[x][y] in valids:
                valids.remove(grid[x][y])

    counts = {v: 0 for v in valids}
    for x in range(max_x):
        for y in range(max_y):
            ic = grid[x][y]
            if ic in valids:
                counts[ic] += 1
    return max(counts.values())


def b(coords):
    max_x = max(c[0] for c in coords) + 1
    max_y = max(c[1] for c in coords) + 1
    grid = [[-1 for _ in range(max_y)] for _ in range(max_x)]
    for x in range(max_x):
        for y in range(max_y):
            grid[x][y] = sum(abs(x - c[0]) + abs(y - c[1]) for c in coords) < 10_000
    return sum(sum(g) for g in grid)


def main():
    coords = [tuple(map(int, line.split(", ")))
              for line in open("input06.txt").read().split("\n")]
    print(b(coords))


if __name__ == "__main__":
    main()
