from collections import defaultdict
import itertools
import sys

sys.setrecursionlimit(10_000)


def parse_part(s):
    nums = list(map(int, s[2:].split("..")))
    return range(nums[0], (nums[1] if len(nums) > 1 else nums[0]) + 1)


def print_world(world):
    xs = [x for x, _ in world.keys()]
    ys = [y for _, y in world.keys()]
    min_x, max_x = min(xs), max(xs)
    min_y, max_y = min(ys), max(ys)
    for y in range(min_y, max_y + 1):
        for x in range(min_x, max_x + 1):
            print(world[x, y], end="")
        print()


visit_count = defaultdict(int)
import time


def spread(world, x, y, min_y, max_y):
    left = x
    blocked_left = False
    while world[left, y + 1] in "#~":
        if world[left - 1, y] not in "#~":
            left -= 1
            world[left, y] = "|"
        else:
            blocked_left = True
            break
    else:
        a = flood(world, left, y + 1, min_y, max_y)
        if a:
            spread(world, left, y, min_y, max_y)

    right = x
    blocked_right = False
    while world[right, y + 1] in "#~":
        if world[right + 1, y] not in "#~":
            right += 1
            world[right, y] = "|"
        else:
            blocked_right = True
            break
    else:
        a = flood(world, right, y + 1, min_y, max_y)
        if a:
            spread(world, right, y, min_y, max_y)
    if blocked_left and blocked_right:
        for x in range(left, right + 1):
            world[x, y] = "~"
        return True
    return True


def flood(world, x, y, min_y, max_y):
    if not min_y <= y <= max_y:
        return False
    if world[x, y] in "#~":
        return True

    # time.sleep(0.2)
    # print_world(world)
    # print("\n\n")
    if True:
        visit_count[x, y] += 1
        if visit_count[x, y] > 100:
            # print_world(world)
            # print(
            #     "Possible loop detected:",
            #     x,
            #     y,
            #     "x col:",
            #     x - min(x for x, _ in world.keys()),
            # )
            return False
            sys.exit()

    world[x, y] = "|"
    blocked = flood(world, x, y + 1, min_y, max_y)

    if blocked and world[x, y + 1] in "#~":
        return spread(world, x, y, min_y, max_y)
    return False


def a(world):
    ys = [y for x, y in world.keys()]
    min_y, max_y = min(ys), max(ys)
    flood(world, 500, 1, min_y, max_y)
    print_world(world)
    real_min_y = min(y for (_, y), v in world.items() if v == "#")
    print(sum(v in "~|" for (_, y), v in world.items() if y >= real_min_y))


def b(world):
    ys = [y for x, y in world.keys()]
    min_y, max_y = min(ys), max(ys)
    flood(world, 500, 1, min_y, max_y)
    print_world(world)
    real_min_y = min(y for (_, y), v in world.items() if v == "#")
    print(sum(v in "~" for (_, y), v in world.items() if y >= real_min_y))


def main():
    lines = open("input17.txt").read().splitlines()
    world = defaultdict(lambda: ".")
    world[500, 0] = "+"
    for line in lines:
        parts = line.split(", ")
        if "x" not in parts[0]:
            parts = parts[::-1]
        x_range = parse_part(parts[0])
        y_range = parse_part(parts[1])
        for x, y in itertools.product(x_range, y_range):
            world[x, y] = "#"
    a(world)


if __name__ == "__main__":
    main()
