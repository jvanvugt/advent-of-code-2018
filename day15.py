from collections import deque
from dataclasses import dataclass
import itertools
from typing import List, Set


@dataclass(frozen=True)
class V2:
    x: int
    y: int

    def __add__(self, other: "V2") -> "V2":
        return V2(self.x+other.x, self.y+other.y)
    
    def __iter__(self):
        yield self.x
        yield self.y


@dataclass
class Unit:
    id: int
    pos: V2
    type: str
    hp: int = 200
    ap: int = 3


def print_state(dungeon: List[str], goblins: List[Unit], elves: List[Unit]) -> None:
    print("="*20)
    l = [list(d) for d in dungeon]
    for u in goblins + elves:
        l[u.pos.y][u.pos.x] = u.type
    l = ["".join(d) for d in l]
    print(*l, sep="\n")


def neighbours(pos: V2) -> List[V2]:
    return [
        # OrderMatters
        pos + V2(0, -1),
        pos + V2(-1, 0),
        pos + V2(1, 0),
        pos + V2(0, 1),
    ]


def find_step(pos: V2, enemies: List[Unit], occupied: Set[V2], dungeon: List[str]) -> V2:
    # End states
    open_squares = {n
                    for e in enemies
                    for n in neighbours(e.pos)
                    if n not in occupied and dungeon[n.y][n.x] == "."}

    enemies_pos = set(e.pos for e in enemies)
    frontier = deque([(n, n, 1) for n in neighbours(pos) if dungeon[n.y][n.x] == "." and n not in occupied])
    visited = {pos}
    max_len = float("inf")
    solutions = []
    if len(open_squares) == 0 or len(frontier) == 0 or pos in open_squares: 
        return
    while len(frontier) > 0:
        current_pos, first_step, length = frontier.popleft()
        if length > max_len:
            break
        if current_pos in open_squares:
            max_len = length
            solutions.append((current_pos, first_step))
        for next_pos in neighbours(current_pos):
            if dungeon[next_pos.y][next_pos.x] == "." and next_pos not in visited and next_pos not in occupied:
                frontier.append((next_pos, first_step, length + 1))
                visited.add(next_pos)
    return min(solutions, key=lambda s: tuple(s[0])[::-1])[1] if solutions else None


def move(unit: Unit, dungeon: List[str], goblins: List[Unit], elves: List[Unit]) -> None:
    enemies = goblins if unit.type == "E" else elves
    occupied = {o.pos for o in goblins + elves if unit is not o}
    new_pos = find_step(unit.pos, enemies, occupied, dungeon)
    if new_pos is not None:
        unit.pos = new_pos


def a(dungeon: List[str], goblins: List[Unit], elves: List[Unit]) -> int:
    for i in itertools.count():
        print_state(dungeon, goblins, elves)
        units = sorted(goblins + elves, key=lambda u: tuple(u.pos)[::-1])
        killed = set()
        for unit in units:
            if unit.id in killed:
                continue
            enemies = goblins if unit.type == "E" else elves
            if len(enemies) == 0:
                print(i, sum((u.hp for u in goblins + elves)))
                return i * sum((u.hp for u in goblins + elves))
            move(unit, dungeon, goblins, elves)

            in_reach = neighbours(unit.pos)
            reachable_enemies = sorted([e for e in enemies if e.pos in in_reach], key=lambda e: in_reach.index(e.pos))
            if not reachable_enemies:
                continue

            target = min(reachable_enemies, key=lambda e: e.hp)
            target.hp -= unit.ap
            if target.hp <= 0:
                enemies.remove(target)
                killed.add(target.id)

def main() -> None:
    for ap in range(4, 1000):
        dungeon = open("input15.txt").read().split("\n")
        goblins = []
        elves = []
        i = 0
        for y, row in enumerate(dungeon):
            for x, l in enumerate(row):
                if l == "G":
                    goblins.append(Unit(id=i, pos=V2(x, y), type=l))
                    i += 1
                elif l == "E":
                    elves.append(Unit(id=i, pos=V2(x, y), type=l, ap=ap))
                    i += 1
        dungeon = [d.replace("G", ".").replace("E", ".") for d in dungeon]
        n_elves = len(elves)
        res = a(dungeon, goblins, elves)
        if len(elves) == n_elves:
            print(res)
            break


if __name__ == "__main__":
    main()
