from collections import defaultdict
import itertools
from heapq import heappush, heappop


def a(edges):
    heap = sorted([task for task, prereqs in edges.items() if len(prereqs) == 0])
    order = []
    while heap:
        task = heappop(heap)
        order.append(task)
        for other, prereqs in edges.items():
            if task in prereqs:
                prereqs.remove(task)
                if len(prereqs) == 0:
                    heappush(heap, other)
    return "".join(order)


def b(edges):
    n_workers = 5
    elves = [0] * n_workers
    assigned = [None] * n_workers
    todo = [task for task, prereqs in edges.items() if len(prereqs) == 0]
    for t in itertools.count():
        if not todo and all(e == 0 for e in elves):
            return t
        available = [i for i, e in enumerate(elves) if e == 0]
        for e in available:
            if not todo:
                break
            task = todo.pop()
            elves[e] += ord(task) - ord("A") + 61
            assigned[e] = task
        for e in range(len(elves)):
            if elves[e] > 0:
                elves[e] -= 1
                if elves[e] == 0:
                    task = assigned[e]
                    for other, prereqs in edges.items():
                        if task in prereqs:
                            prereqs.remove(task)
                            if len(prereqs) == 0:
                                todo.append(other)


def main():
    lines = open("input07.txt").read().split("\n")
    lines = [l.split(" ") for l in lines]
    from_to = [(l[1], l[7]) for l in lines]
    edges = defaultdict(set)
    for prereq, task in from_to:
        edges[task].add(prereq)
        if prereq not in edges:
            edges[prereq]
    print(b(edges))


if __name__ == "__main__":
    main()
