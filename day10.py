from dataclasses import dataclass
import itertools
import re
import random


@dataclass
class Point:
    position: complex
    velocity: complex


def print_points(points):
    min_x = int(min(p.position.real for p in points))
    max_x = int(max(p.position.real for p in points))
    min_y = int(min(p.position.imag for p in points))
    max_y = int(max(p.position.imag for p in points))
    field = [[" " for _ in range(max_x - min_x + 1)] for _ in range(max_y - min_y + 1)]
    for p in points:
        y, x = int(p.position.imag - min_y), int(p.position.real - min_x)
        field[y][x] = "#"
    print("\n".join(["".join(f) for f in field]))


def a(points):
    rands = random.choices(points, k=10)
    max_dist = float("inf")
    for i in itertools.count():
        for point in points:
            point.position += point.velocity
        diffs = (p2.position - p1.position for p1, p2 in itertools.product(rands, repeat=2))
        dist = sum(abs(d.real) + abs(d.imag) for d in diffs)
        if dist > max_dist:
            for point in points:
                point.position -= point.velocity
            break
        else:
            max_dist = dist
    print_points(points)
    return i


def main():
    lines = open("input10.txt").read().split("\n")
    points = [list(map(int, re.findall(r"-?\d+", l))) for l in lines]
    points = [Point(complex(p[0], p[1]), complex(p[2], p[3])) for p in points]
    print(a(points))


if __name__ == "__main__":
    main()
