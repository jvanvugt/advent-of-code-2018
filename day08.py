def a(nums):
    if len(nums) == 0:
        return [], 0
    num_children, num_metadata = nums[:2]
    rest = nums[2:]
    meta_sums = 0
    for _ in range(num_children):
        rest, meta = a(rest)
        meta_sums += meta
    meta_sums += sum(rest[:num_metadata])
    rest = rest[num_metadata:]
    return rest, meta_sums


def b(nums):
    if len(nums) == 0:
        return [], 0
    num_children, num_metadata = nums[:2]
    rest = nums[2:]
    if num_children == 0:
        return rest[num_metadata:], sum(rest[:num_metadata])
    child_values = []
    for _ in range(num_children):
        rest, value = b(rest)
        child_values.append(value)
    value = sum(child_values[i-1] for i in rest[:num_metadata] if i <= len(child_values))
    rest = rest[num_metadata:]
    return rest, value


def main():
    nums = list(map(int, open("input08.txt").read().split()))
    print(a(nums))


if __name__ == "__main__":
    main()
