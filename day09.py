from dataclasses import dataclass


def a(num_players, last_score):
    circle = [0]
    scores = [0] * num_players
    current_player = 0
    current_marble = 0
    for i in range(1, last_score + 1):
        if i % (last_score / 100) == 0:
            print(i)
        if i % 23 == 0:
            current_marble = (current_marble - 7) % len(circle)
            scores[current_player] += i + circle.pop(current_marble)
        else:
            circle.insert(current_marble, )
        current_player = (current_player + 1) % num_players
    return max(scores)


@dataclass
class Node:
    value: int
    prev: "Node"
    next: "Node"


def b(num_players, last_score):
    scores = [0] * num_players
    current_player = 0
    current_marble = Node(0, None, None)
    current_marble.prev = current_marble
    current_marble.next = current_marble
    for i in range(1, last_score + 1):
        if i % 23 == 0:
            for _ in range(7):
                current_marble = current_marble.prev
            scores[current_player] += i + current_marble.value
            current_marble.prev.next = current_marble.next
            current_marble.next.prev = current_marble.prev
            current_marble = current_marble.next
        else:
            current_marble = current_marble.next
            current_marble = Node(i, current_marble, current_marble.next)
            current_marble.prev.next = current_marble
            current_marble.next.prev = current_marble
        current_player = (current_player + 1) % num_players
    return max(scores)


def main():
    open("input09.txt")
    print(b(448, 71628 * 100))


if __name__ == "__main__":
    main()
