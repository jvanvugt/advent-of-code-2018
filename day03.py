from collections import defaultdict
import itertools
import re


def a(squares):
    res = defaultdict(int)
    for _, x, y, w, h in squares:
        for x, y in itertools.product(range(x, x+w), range(y, y+h)):
            res[(x, y)] += 1
    return sum(v > 1 for v in res.values())


def b(squares):
    res = defaultdict(set)
    valids = set(square[0] for square in squares)
    for claim_id, x, y, w, h in squares:
        for x, y in itertools.product(range(x, x+w), range(y, y+h)):
            if res[(x, y)]:
                for other_claim in res[(x, y)]:
                    if other_claim in valids:
                        valids.remove(other_claim)
                if claim_id in valids:
                    valids.remove(claim_id)
            res[(x, y)].add(claim_id)
    return valids


if __name__  == "__main__":
    input_pattern = re.compile("#(\d+) @ (\d+),(\d+): (\d+)x(\d+)")
    squares = [list(map(int, input_pattern.match(line).groups())) for line in open("input03.txt")]
    print(b(squares))
