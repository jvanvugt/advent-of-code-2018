function arithr(op)
    function helpera(registers, instr)
        registers[instr[4]+1] = op(registers[instr[2]+1], registers[instr[3]+1])
    end
end

function arithi(op)
    function helperb(registers, instr)
        registers[instr[4]+1] = op(registers[instr[2]+1], instr[3])
    end
end

function arithir(op)
    function helperc(registers, instr)
        registers[instr[4]+1] = op(instr[2], registers[instr[3]+1])
    end
end


addr = arithr(+)
addi = arithi(+)
mulr = arithr(*)
muli = arithi(*)
banr = arithr(&)
bani = arithi(&)
borr = arithr(|)
bori = arithi(|)

function setr(registers, instr)
    registers[instr[4]+1] = registers[instr[2]+1]
end

function seti(registers, instr)
    registers[instr[4]+1] = instr[2]
end

gtir = arithir(>)
gtri = arithi(>)
gtrr = arithr(>)
eqir = arithir(==)
eqri = arithi(==)
eqrr = arithr(==)

all_ops = [addr, addi, mulr, muli, banr, bani, borr, bori, setr, seti, gtir, gtri, gtrr, eqir, eqri, eqrr]

function a()
    contents = open("input16a.txt") do file
        read(file, String)
    end
    lines = split(contents, "\n")

    more_than_three = 0
    possible_ops = [Set(1:16) for _ in 1:16]
    for i = 1:4:length(lines)
        before = map(x->parse(Int32, x), split(chomp(split(lines[i], ": ")[2])[2:end-2], ","))
        instr = map(x->parse(Int32, x), split(chomp(lines[i+1])))
        after = map(x->parse(Int32, x), split(chomp(split(lines[i+2], ": ")[2])[3:end-2], ","))

        num_similar = 0
        for (index, op) in enumerate(all_ops)
            registers = copy(before)
            op(registers, instr)
            if registers == after
                num_similar += 1
            else
                if in(index, possible_ops[instr[1]+1])
                    delete!(possible_ops[instr[1]+1], index)
                end
            end
        end
        more_than_three += num_similar >= 3
    end
    println(more_than_three)
    for pops in possible_ops
        println(pops)
    end
end

function b()
    contents = open("input16b.txt") do file
        read(file, String)
    end
    lines = split(contents, "\n")
    translation = [5, 16, 9, 14, 8, 4, 6, 7, 11, 13, 2, 12, 15, 1, 3, 10]
    registers = [0, 0, 0, 0]
    for line in lines
        instr = map(x->parse(Int32, x), split(chomp(line)))
        all_ops[translation[instr[1]+1]](registers, instr)
    end
    println(registers[1])
end

# a()
b()