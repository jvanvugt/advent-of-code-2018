from collections import deque, defaultdict
import sys

sys.setrecursionlimit(10000)


def show_map(m):
    ys = [y for (y, _) in m.keys()]
    xs = [x for (_, x) in m.keys()]
    min_x, max_x, min_y, max_y = min(xs), max(xs), min(ys), max(ys)
    ss = [["#" for _ in range(max_x - min_x + 1)] for _ in range(max_y - min_y + 1)]
    for (y, x), v in m.items():
        ss[y - min_y][x - min_x] = v
    print("\n".join(["".join(s) for s in ss]))


def find_close(regex, begin):
    c = 1
    splits = [begin]
    while c != 0:
        begin += 1
        if c == 1 and regex[begin] == "|":
            splits.append(begin)
        if regex[begin] == "(":
            c += 1
        elif regex[begin] == ")":
            c -= 1
    return begin, splits


states = set()


def build_map(regex, cur_x, cur_y, m):
    # This state thing is a hack and probably circumvents some stupid bug
    state = (regex, cur_x, cur_y)
    if state in states:
        return
    else:
        states.add(state)
    for i in range(len(regex)):
        if regex[i] == "N":
            m[cur_y - 1, cur_x] = "-"
            m[cur_y - 2, cur_x] = "."
            cur_y -= 2
        elif regex[i] == "E":
            m[cur_y, cur_x + 1] = "|"
            m[cur_y, cur_x + 2] = "."
            cur_x += 2
        elif regex[i] == "S":
            m[cur_y + 1, cur_x] = "-"
            m[cur_y + 2, cur_x] = "."
            cur_y += 2
        elif regex[i] == "W":
            m[cur_y, cur_x - 1] = "|"
            m[cur_y, cur_x - 2] = "."
            cur_x -= 2
        elif regex[i] == "(":
            nend, splits = find_close(regex, i)
            splits.append(nend)
            for sbegin, send in zip(splits, splits[1:]):
                nreg = regex[sbegin + 1 : send] + regex[nend + 1 :]
                build_map(nreg, cur_x, cur_y, m)
            return
        else:
            raise ValueError(f"{regex[i], i}\nFull:{regex}")


def a(m):
    q = deque([((0, 0), 0)])
    visited = {(0, 0)}
    cur_x = cur_y = 0
    while len(q) > 0:
        (cur_y, cur_x), s = q.popleft()
        for dy, dx in [(-1, 0), (0, -1), (1, 0), (0, 1)]:
            if m[cur_y + dy, cur_x + dx] in "|-":
                nyx = cur_y + 2 * dy, cur_x + 2 * dx
                if nyx not in visited:
                    q.append((nyx, s + 1))
                    visited.add(nyx)
    return s


def b(m):
    n = 0
    q = deque([((0, 0), 0)])
    visited = {(0, 0)}
    cur_x = cur_y = 0
    while len(q) > 0:
        (cur_y, cur_x), s = q.popleft()
        if s >= 1000:
            n += 1
        for dy, dx in [(-1, 0), (0, -1), (1, 0), (0, 1)]:
            if m[cur_y + dy, cur_x + dx] in "|-":
                nyx = cur_y + 2 * dy, cur_x + 2 * dx
                if nyx not in visited:
                    q.append((nyx, s + 1))
                    visited.add(nyx)
    return n


def main():
    regex = open("input20.txt").read()[1:-1]
    m = defaultdict(lambda: "#")
    m[0, 0] = "."
    build_map(regex, 0, 0, m)
    print("Made map", flush=True)
    show_map(m)
    print(b(m))


if __name__ == "__main__":
    main()
