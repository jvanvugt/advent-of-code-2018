def a(state, rules, num_iter=20):
    offset = 0
    for i in range(num_iter):
        state = "...." + state + "...."
        state = "".join([rules.get(state[i:i+5], ".") for i in range(len(state)-5)])
        offset += 2 - state.find("#")
        state = state.strip(".")
    return sum(i - offset for i in range(len(state)) if state[i] == "#")


def b():
    n = 50_000_000_000 - 1000 + 1
    return 51832 + 51 * n


def main():
    lines = open("input12.txt").read().split("\n")
    initial_state = lines[0].split(": ")[1]
    rules = {l.split(" => ")[0]: l.split(" => ")[1] for l in lines[2:]}
    # print(a(initial_state, rules))
    print(b())


if __name__ == "__main__":
    main()
